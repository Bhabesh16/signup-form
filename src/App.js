import React from 'react';

import Form from './components/Form';

class App extends React.Component {
  render() {
    return (
      <section className="vh-100 ">
        <div className="container h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <Form />
          </div>
        </div>
      </section>
    );
  }
}

export default App;